package com.choco.zookeeper;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

public class ChocoZookeeperSimpleExample {
	public static void main(String[] args) throws Exception {
		new ChocoZookeeperSimpleExample().go();
	}

	private void go() throws Exception {
		CuratorFramework client = getCuratorFramework();

		byte[] myData = "choco_data".getBytes();
		client.create().forPath("/choco", myData);

		System.out.println("/choco created.");

		byte[] bytes = client.getData().forPath("/choco");
		System.out.println("result : " + new String(bytes));

		client.delete().forPath("/choco");
		System.out.println("delete /choco");

		client.close();
	}

	private CuratorFramework getCuratorFramework() {
		String zookeeperConnectionString = "52.79.215.20:2181,52.79.103.111:2181,52.79.112.202:2181";

		RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
		CuratorFramework client = CuratorFrameworkFactory.newClient(zookeeperConnectionString, retryPolicy);

		client.start();
		System.out.println("client start.");

		return client;
	}
}
