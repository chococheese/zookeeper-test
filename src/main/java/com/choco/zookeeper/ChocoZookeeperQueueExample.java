package com.choco.zookeeper;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.queue.DistributedQueue;
import org.apache.curator.framework.recipes.queue.QueueBuilder;
import org.apache.curator.framework.recipes.queue.QueueConsumer;
import org.apache.curator.framework.recipes.queue.QueueSerializer;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.retry.ExponentialBackoffRetry;

public class ChocoZookeeperQueueExample {
	private static final ObjectMapper OBJECT_MAPPER;

	static {
		OBJECT_MAPPER = new ObjectMapper();
		OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public static void main(String[] args) throws Exception {
		new ChocoZookeeperQueueExample().distributedQueueTest();
	}

	private void distributedQueueTest() throws Exception {
		CuratorFramework client = getCuratorFramework();
		QueueConsumer<ChocoMessage> consumer = getQueueConsumer();
		QueueSerializer<ChocoMessage> serializer = getQueueSerializer();
		String path = "/choco_queue";

		QueueBuilder<ChocoMessage> builder = QueueBuilder.builder(client, consumer, serializer, path);
		DistributedQueue<ChocoMessage> queue = builder.buildQueue();

		ChocoMessage chocoMessage = ChocoMessage.builder()
			.id(1L)
			.message("choco message hello~")
			.build();

		queue.start();
		queue.put(chocoMessage);
		queue.close();
	}

	private CuratorFramework getCuratorFramework() {
		String zookeeperConnectionString = "52.79.215.20:2181,52.79.103.111:2181,52.79.112.202:2181";

		RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
		CuratorFramework client = CuratorFrameworkFactory.newClient(zookeeperConnectionString, retryPolicy);

		client.start();
		System.out.println("client start.");

		return client;
	}

	private QueueConsumer<ChocoMessage> getQueueConsumer() {
		return new QueueConsumer<ChocoMessage>() {
			@Override
			public void consumeMessage(ChocoMessage message) throws Exception {
				System.out.println("consume message : " + message);
			}

			@Override
			public void stateChanged(CuratorFramework client, ConnectionState newState) {
				System.out.println("state changed. newState : " + newState.name());
			}
		};
	}

	private QueueSerializer<ChocoMessage> getQueueSerializer() {
		return new QueueSerializer<ChocoMessage>() {
			@Override
			public byte[] serialize(ChocoMessage item) {
				try {
					return OBJECT_MAPPER.writeValueAsBytes(item);
				} catch (Exception e) {
					return null;
				}
			}

			@Override
			public ChocoMessage deserialize(byte[] bytes) {
				try {
					String chocoMessageStr = new String(bytes);
					return OBJECT_MAPPER.readValue(chocoMessageStr, ChocoMessage.class);
				} catch (Exception e) {
					return null;
				}
			}
		};
	}

	@Getter
	@Setter
	@ToString
	@Builder
	public static class ChocoMessage {
		private Long id;
		private String message;
	}
}
